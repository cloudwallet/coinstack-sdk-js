# CoinStack SDK for Javascript

[![NPM](https://nodei.co/npm/coinstack-sdk-js.svg?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/coinstack-sdk-js/)

The official CoinStack SDK for Javascript.

## Installation
### In Node.js
The preferred way to install the CoinStack SDK for Node.js is to use the
[npm](http://npmjs.org) package manager for Node.js. Simply type the following
into a terminal window:

```sh
npm install coinstack-sdk-js
```

## Usage
### Registration
Appropriate credentials must be created prior to using CoinStack SDK for Node.js; credentials can be created using CoinStack dashboard (https://www.blocko.io/)
### Initialization
Start by importing coinstack package
```js
var CoinStack = require('coinstack-sdk-js')
```

You can provide credentials through constructor or environment varaiables
```sh
export COINSTACK_ACCESS_KEY="YOUR_COINSTACK_ACCESS_KEY"
export COINSTACK_SECRET_KEY="YOUR_COINSTACK_SECRET_KEY"
```
```js
var client = new CoinStack()
```
or
```js
var accessKey = "YOUR_COINSTACK_ACCESS_KEY"
var secretKey = "YOUR_COINSTACK_SECRET_KEY"
var client = new CoinStack(accessKey, secretKey)
```
### API Usage
```js
// get best block 
client.getBlockchainStatus(function(err, status) {
    console.log(status.best_block_hash, status.best_height);
});

// check balance
client.getBalance("YOUR_BLOCKCHAIN_ADDRESS", function(err, balance) {
    console.log(balance);
});

// creating tx
var txBuilder = client.createTransactionBuilder();
txBuilder.addOutput("TO_ADDRESS1", CoinStack.Math.toSatoshi("0.01"))
txBuilder.addOutput("TO_ADDRESS2", CoinStack.Math.toSatoshi("0.03"))
txBuilder.setInput("MY_WALLET_ADDRESS");
txBuilder.buildTransaction(function(err, tx) {
    tx.sign("MY_PRIVATE_KEY_WIF")
    var rawTx = tx.serialize()
    // send tx
    client.sendTransaction(rawTx, function(err) {
        if (null != err) {
            console.log("failed to send tx");
        }
    );
})
```
## Further Information
Visit https://www.blocko.io/ for registration and management.

## License
Licensed under the MIT License.