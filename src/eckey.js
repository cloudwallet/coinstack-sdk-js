var bitcoin = require('bitcoinjs-lib')
var crypto = require('crypto')
var Util = require('./util.js')

	
var ECKey = {

}
ECKey.createKey = function() {
    return Util.bitcoin().ECKey.makeRandom().toWIF();
};

ECKey.deriveAddress = function(privateKeyWIF) {
    var key = Util.bitcoin().ECKey.fromWIF(privateKeyWIF);
    return key.pub.getAddress().toString();
};

ECKey.derivePubKey = function(privateKeyWIF) {
    var key = Util.bitcoin().ECKey.fromWIF(privateKeyWIF);
    return key.pub.toHex();
};

ECKey.validateAddress = function(address) {
    try {
        var adr = new Util.bitcoin().Address.fromBase58Check(address);
        if (adr.version != Util.bitcoin().networks.bitcoin.pubKeyHash &&
            adr.version != Util.bitcoin().networks.bitcoin.scriptHash) {
            return false;
        }
        return true;
    } catch (e) {
        // ignore errors
        return false;
    }
};


module.exports = ECKey;