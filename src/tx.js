var util = require('./util.js')
var _ = require('underscore');

function Transaction(bitcoinTx) {
    this.bitcoinTx = bitcoinTx
    this.builtTx = null
}

Transaction.fromHex = function(hex) {
    var tx = new Transaction(null)
    tx.builtTx = util.bitcoin().Transaction.fromHex(hex)
    tx.bitcoinTx = util.bitcoin().TransactionBuilder.fromTransaction(tx.builtTx)
    return tx
}

Transaction.prototype.sign = function(privateKeyWIF) {
    var keyPair = util.bitcoin().ECKey.fromWIF(privateKeyWIF)
    var bitcoinTx = this.bitcoinTx
    if (this.bitcoinTx.inputs) {
        _.each(this.bitcoinTx.inputs, function(input, index) {
            bitcoinTx.sign(index, keyPair);
        })
    } else { // FIXME: fall back for older version of bitcoinjs (workaround for bitcoinjs-meteor browserify-minify conflict)
        _.each(this.bitcoinTx.tx.ins, function(input, index) {
            bitcoinTx.sign(index, keyPair);
        });
    }
    this.builtTx = this.bitcoinTx.build()
}

Transaction.prototype.signMultisig = function(privateKeyWIFs, redeemScript) {
    var redeem = util.bitcoin().Script.fromHex(redeemScript)
    var bitcoinTx = this.bitcoinTx
    _.each(this.bitcoinTx.inputs, function(input, index) {
        _.each(privateKeyWIFs, function(privatekey, index2) {
            var keyPair = util.bitcoin().ECKey.fromWIF(privatekey)
            bitcoinTx.sign(index, keyPair, redeem);
        })
    })

    this.builtTx = this.bitcoinTx.build()
}

Transaction.prototype.signIncompleteMultisig = function(privateKeyWIFs, redeemScript) {
    var redeem = util.bitcoin().Script.fromHex(redeemScript)
    var bitcoinTx = this.bitcoinTx
    _.each(this.bitcoinTx.inputs, function(input, index) {
        _.each(privateKeyWIFs, function(privatekey, index2) {
            var keyPair = util.bitcoin().ECKey.fromWIF(privatekey)
            bitcoinTx.sign(index, keyPair, redeem);
        })
    })

    this.builtTx = this.bitcoinTx.buildIncomplete()
}

Transaction.prototype.getHash = function() {
    return util.convertEndianness(this.builtTx.getHash().toString('hex'))
}

Transaction.prototype.serialize = function() {
    return this.builtTx.toHex()
}

module.exports = Transaction;