var bitcoin = require('bitcoinjs-lib')
var elliptic = require('elliptic')
var crypto = require('crypto')
var Util = require('./util.js')
var bufferutils = bitcoin.bufferutils;
var BigInteger = require('bigi')
var ecdsa = bitcoin.ecdsa;
var ecurve = require('ecurve');
var ECPubKey = bitcoin.ECPubKey;
var ecparams = ecurve.getCurveByName('secp256k1')

function magicHash(message, network) {
	var magicPrefix = new Buffer(network.magicPrefix)
	var messageBuffer = new Buffer(message)
	var lengthBuffer = bufferutils.varIntBuffer(messageBuffer.length)

	var buffer = Buffer.concat([magicPrefix, lengthBuffer, messageBuffer])
	return bitcoin.crypto.hash256(buffer)
}


var EC = elliptic.ec;
var ec = new EC('secp256k1');

function sign(privKey, hash) {
	var keyHex = privKey.d.toHex();
	var key = ec.keyFromPrivate(keyHex, 'hex'); // converted key
	var signature = privKey.sign(hash);
	return signature.toDER().toString('hex');
}

function signMessage(privKey, message, network) {
	network = network || networks.bitcoin
	var keyHex = privKey.d.toHex();
	var key = ec.keyFromPrivate(keyHex, 'hex'); // converted key
	var hash = magicHash(message, network)
	var unprocessedSignature = key.sign(hash);
	var signatureBuffer = new Buffer(unprocessedSignature.toDER());
	var signature = bitcoin.ECSignature.fromDER(signatureBuffer);
	var i = ec.getKeyRecoveryParam(hash, unprocessedSignature, key.getPublic());
	return signature.toCompact(i, privKey.pub.compressed)
}

function verifyMessage(address, signature, message, network) {
	if (!Buffer.isBuffer(signature)) {
		signature = new Buffer(signature, 'base64')
	}

	network = network || networks.bitcoin

	var hash = magicHash(message, network)
	var parsed = bitcoin.ECSignature.parseCompact(signature)
	var e = BigInteger.fromBuffer(hash)
	var Q = ec.recoverPubKey(hash, {
		r: parsed.signature.r.toString(16),
		s: parsed.signature.s.toString(16)
	}, parsed.i);
	var pubKey = ECPubKey.fromBuffer(new Buffer(Q.encodeCompressed()));
	var recoveredAddress = pubKey.getAddress(network).toString();
	return recoveredAddress === address.toString()
}


var ECDSA = {

}

ECDSA.sign = function (privateKeyWIF, payload) {
	var hash = crypto.createHash('sha256').update(payload).digest()
	var privKey = Util.bitcoin().ECKey.fromWIF(privateKeyWIF);
	var signature = sign(privKey, hash)
	return signature;
};

ECDSA.signMessage = function (privateKeyWIF, messageText) {
	var networks = Util.bitcoin().networks;
	var network = networks['bitcoin'];

	var privKey = Util.bitcoin().ECKey.fromWIF(privateKeyWIF);
	var signature = signMessage(privKey, messageText, network)

	return signature.toString('base64')
};

ECDSA.verifyMessageSignature = function (address, messageText, signature) {
	var networks = Util.bitcoin().networks;
	var network = networks['bitcoin'];

	var addr = Util.bitcoin().Address.fromBase58Check(address);
	var res = verifyMessage(addr, signature, messageText, network)

	return res
};

ECDSA.verifySignature = function (pubkey, payload, signature) {
	var hash = crypto.createHash('sha256').update(payload).digest()
	var ecpubkey = ec.keyFromPublic(pubkey, 'hex');
	var sig = new Buffer(signature, 'hex')
	return ecpubkey.verify(hash, sig);
};

module.exports = ECDSA;