var Promise = require('promise');
var crypto = require('crypto')
var TransactionBuilder = require('./tx_builder.js')
var BitcoinMath = require('./math.js')

function LuaContractBuilder(client) {
    this.client = client
    this.fee = BitcoinMath.toSatoshi("0.0001") // default fee
    return this;
}

LuaContractBuilder.prototype.setInput = function (fromAddress) {
    this.fromAddress = fromAddress;
    return this;
}

LuaContractBuilder.prototype.setContractID = function (contractID) {
    this.contractID = contractID;
    return this
}

LuaContractBuilder.prototype.setFee = function (amount) {
    this.fee = amount;
    return this
}

LuaContractBuilder.prototype.setDefinition = function (body) {
    if (this.definition || this.execution) {
        throw new Error("Duplicate definition")
    }
    this.definition = true;
    this.body = "{\"type\":\"LSC\",\"body\":\"" + new Buffer(body).toString('base64') + "\"}";
    this.body = "data:text/plain;charset=utf-8;base64," + new Buffer(this.body).toString('base64');
    this.hash = crypto.createHash("sha256").update(this.body, 'utf8').digest().toString('hex');
    return this;
}

LuaContractBuilder.prototype.setExecution = function (body) {
    if (this.definition || this.execution) {
        throw new Error("Duplicate execution")
    }
    this.execution = true;
    this.body = "{\"type\":\"LSC\",\"body\":\"" + new Buffer(body).toString('base64') + "\"}";
    this.body = "data:text/plain;charset=utf-8;base64," + new Buffer(this.body).toString('base64');
    this.hash = crypto.createHash("sha256").update(this.body, 'utf8').digest().toString('hex');
    return this;
}

LuaContractBuilder.prototype.getHash = function () {
    return this.hash;
}

LuaContractBuilder.prototype.buildTransaction = function (callback) {
    var txBuilder = this.client.createTransactionBuilder();
    txBuilder.setInput(this.fromAddress);
    // create data payload
    var payload = "4f43" + "0100";
    if (this.definition) {
        payload += "0001";
    } else if (this.execution) {
        payload += "0002";
    } else {
        throw new Error("Contract definition or execution not defined")
    }

    payload = payload + this.hash + new Buffer(this.body, 'utf8').toString('hex');

    txBuilder.addOutput(this.contractID, BitcoinMath.toSatoshi(0.0001));
    txBuilder.setDataOutput(payload);
    txBuilder.setFee(this.fee);

    return txBuilder.buildTransaction(callback);
}

module.exports = LuaContractBuilder;