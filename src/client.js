var crypto = require('crypto')
var axios = require('axios')
var moment = require('../lib/moment.min.js')
var TransactionBuilder = require('./tx_builder.js')
var LuaContractBuilder = require('./luacontract_builder.js')
var util = require('./util.js')
var ECDSA = require('./ecdsa.js')
var ECKey = require('./eckey.js')
var BitcoinMath = require('./math.js')
var isNode = require('detect-node');
var Transaction = require('./tx.js');
var _ = require('underscore');
var Promise = require('promise');

function convertEndianness(hex) {
    hex = hex.replace(/^(.(..)*)$/, "0$1"); // add a leading zero if needed
    var res = hex.match(/../g); // split number in groups of two
    res.reverse(); // reverse the groups
    return res.join("");
}

function Client(apiKey, secretKey, endpoint, protocol, options) {
    if (apiKey != null) {
        this.apiKey = apiKey;
    } else {
        this.apiKey = process.env.COINSTACK_ACCESS_KEY
    }
    if (secretKey != null) {
        this.secretKey = secretKey;
    } else {
        this.secretKey = process.env.COINSTACK_SECRET_KEY
    }

    if (this.apiKey == null || this.secretKey == null) {
        throw new Error("Invalid credentials provided")
    }

    if (protocol != null) {
        this.protocol = protocol + "://"
    } else {
        this.protocol = "https://";
    }

    if (endpoint != null) {
        this.endpoint = endpoint
    } else {
        this.endpoint = "mainnet.cloudwallet.io";
    }

    if (options != null) {
        if (options.cacheUtxo != null && options.cacheUtxo == true) {
            this.cacheUtxo = true;
            this.utxoCache = {};
        } else {
            this.cacheUtxo = false;
        }
    }

    this.isBrowser = !isNode
}

// helper functions
function newCoinstackError(coinstackErr, status) {
    var err = new Error();
    err.errorType = coinstackErr.error_type;
    err.errorCode = coinstackErr.error_code;
    err.status = status;
    err.message = coinstackErr.error_message;
    err.retry = coinstackErr.retry;

    if (!(coinstackErr.error_cause === undefined)) {
        err.cause = coinstackErr.error_cause;
    }
    return err;
}

function generateAuthString(method, host, suburl, timestamp, contentmd5, contenttype) {
    var authString = ""
    authString += method + "\n";
    authString += host + "\n";
    authString += suburl + "\n";
    authString += timestamp + "\n";
    authString += contentmd5 + "\n";
    authString += contenttype + "\n";
    return authString;
}

Client.prototype.generateOption = function (suburl) {
    var method = "GET";
    var timestamp = moment().format("YYYY-MM-DDTHH:mm:ssZ")
    var url = this.endpoint
    var protocol = this.protocol
    var authString = generateAuthString(method, url, suburl, timestamp, "", "application/json")
    var signature = crypto.createHmac('sha256', this.secretKey).update(authString).digest('base64')
    var authHeader = "APIKey=" + this.apiKey + ",Signature=" + signature + ",Timestamp=" + timestamp

    if (this.isBrowser) {
        // avoid using auth header in order to prevent CORS preflight from browser
        var options = {
            url: protocol + url + suburl,
            method: method.toLowerCase(),
            params: {
                "auth": authHeader,
                "content-type": "application/json"
            }
        };
        return options;
    } else {
        var options = {
            url: protocol + url + suburl,
            method: method.toLowerCase(),
            headers: {
                "Authorization": authHeader,
                "Content-Type": "application/json"
            }
        };
        return options;
    }
}

Client.prototype.generatePostOption = function (suburl, payload) {
    var payloadMd5 = crypto.createHash('md5').update(payload).digest('base64');

    var method = "POST";
    var url = this.endpoint
    var protocol = this.protocol
    var timestamp = moment().format("YYYY-MM-DDTHH:mm:ssZ")
    var authString = generateAuthString(method, url, suburl, timestamp, payloadMd5, "application/json")
    var signature = crypto.createHmac('sha256', this.secretKey).update(authString).digest('base64')
    var authHeader = "APIKey=" + this.apiKey + ",Signature=" + signature + ",Timestamp=" + timestamp


    if (this.isBrowser) {
        var options = {
            url: protocol + url + suburl,
            method: method.toLowerCase(),
            data: payload,
            params: {
                "auth": authHeader,
                "md5": payloadMd5,
                "content-type": "application/json"
            }
        };
        return options;
    } else {
        var options = {
            url: protocol + url + suburl,
            method: method.toLowerCase(),
            headers: {
                "Content-Type": "application/json",
                "Content-MD5": payloadMd5,
                "Authorization": authHeader
            },
            data: payload
        };
        return options;
    }
}

// coinstack REST API
Client.prototype.getBlockchainStatus = function (callback) {
    var suburl = "/blockchain";
    var option = this.generateOption(suburl);
    if (callback) {
        axios(option)
            .then(function (res) {
                callback(null, res.data);
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        // return promise
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    fulfill(res.data);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.prototype.getBlock = function (blockId, callback) {
    var suburl = "/blocks/" + blockId;
    var option = this.generateOption(suburl);
    if (callback) {
        axios(option)
            .then(function (res) {
                callback(null, res.data);
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    fulfill(res.data);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.prototype.getTransaction = function (txId, callback) {
    var suburl = "/transactions/" + txId;
    var option = this.generateOption(suburl);
    if (callback) {
        axios(this.generateOption(suburl))
            .then(function (res) {
                // process outputs to extract OP_RETURN data
                _.each(res.data.outputs, function (output) {
                    var script = util.bitcoin().Script.fromHex(output.script);
                    if (script.chunks.length > 1 && script.chunks[0] == util.bitcoin().opcodes.OP_RETURN) {
                        output.data = script.chunks[1].toString('hex')
                    }
                })
                callback(null, res.data);
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    // process outputs to extract OP_RETURN data
                    _.each(res.data.outputs, function (output) {
                        var script = util.bitcoin().Script.fromHex(output.script);
                        if (script.chunks.length > 1 && script.chunks[0] == util.bitcoin().opcodes.OP_RETURN) {
                            output.data = script.chunks[1].toString('hex')
                        }
                    })
                    fulfill(res.data);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.prototype.getBalance = function (address, callback) {
    var suburl = "/addresses/" + address + "/balance";
    var option = this.generateOption(suburl);
    if (callback) {
        axios(option)
            .then(function (res) {
                callback(null, res.data.balance);
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    fulfill(res.data.balance);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.prototype.getTransactions = function (address, options, callback) {
    if (!callback && typeof options === "function") {
        // support (method, url, callback) argument list
        callback = options;
        options = null;
    }

    var suburl = "/addresses/" + address + "/history";
    var option = this.generateOption(suburl);
    if (options) {
        if (!option.params) {
            option.params = {}
        }
        if (options.limit !== null) {
            option.params.limit = options.limit;
        }
        if (options.skip !== null) {
            option.params.skip = options.skip;
        }
    }
    if (callback) {
        axios(option)
            .then(function (res) {
                callback(null, res.data);
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    fulfill(res.data);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.prototype.getUnspentOutputs = function (address, callback) {
    var suburl = "/addresses/" + address + "/unspentoutputs";
    var option = this.generateOption(suburl);
    var doCache = this.cacheUtxo
    var cache = this.utxoCache
    if (callback) {
        if (doCache) {
            var cachedData = cache[address];
            if (cachedData != null) {
                //FIXME: cache invalidation
                callback(null, cachedData);
                return;
            }
        }

        axios(option)
            .then(function (res) {
                if (doCache) {
                    cache[address] = res.data;
                }
                callback(null, res.data);
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            if (this.cacheUtxo) {
                var cachedData = this.utxoCache[address];
                if (cachedData != null) {
                    //FIXME: cache invalidation
                    fulfill(cachedData);
                    return;
                }
            }

            axios(option)
                .then(function (res) {
                    if (doCache)
                        cache[address] = res.data;
                    fulfill(res.data);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

evitCache = function (cacheMap, txHash, index) {
    var keys = _.keys(cacheMap);
    for (var j = 0; j < keys.length; j++) {
        var key = keys[j];
        var cache = cacheMap[key];
        for (var i = 0; i < cache.length; i++) {
            var cacheItem = cache[i];
            if (cacheItem.transaction_hash == txHash && cacheItem.index == index) {
                cache.splice(i,1);
                return;
            }
        }
    }
}

cacheItem = function (cache, txHash, index, value, script) {
    cache.push({
        transaction_hash: txHash,
        index: index,
        value: value,
        script: script,
        confirmations: 0
    })
}

Client.prototype.sendTransaction = function (rawTx, callback) {
    var suburl = "/transactions";
    var payload = JSON.stringify({
        tx: rawTx
    })

    var option = this.generatePostOption(suburl, payload);
    var doCache = this.cacheUtxo;
    var cache = this.utxoCache;
    if (callback) {
        axios(option)
            .then(function (res) {
                if (doCache) {
                    // update cache
                    var bitcoin = util.bitcoin()
                    var tx = Transaction.fromHex(rawTx);
                    var txHash = tx.getHash();

                    for (var i = 0; i < tx.builtTx.ins.length; i++) {
                        var input = tx.builtTx.ins[i];
                        evitCache(cache, input.hash.toString('hex'), input.index);
                    }

                    txHash = convertEndianness(txHash);

                    for (var i = 0; i < tx.builtTx.outs.length; i++) {
                        var output = tx.builtTx.outs[i];
                        var address = null;
                        
                        try {
                            address = bitcoin.Address.fromOutputScript(output.script).toString();
                        } catch (e) {
                            continue;
                        }
                        var addressCache = cache[address];
                        if (addressCache == null) {
                            addressCache = [];
                            cache[address] = addressCache;
                        }
                        cacheItem(addressCache, txHash, i, output.value + "", output.script.buffer.toString("hex"));
                    }
                }
                callback(null, res.data);
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    if (doCache) {
                        // update cache
                        var bitcoin = util.bitcoin()
                        var tx = Transaction.fromHex(rawTx);
                        var txHash = tx.getHash();

                        for (var i = 0; i < tx.builtTx.ins.length; i++) {
                            var input = tx.builtTx.ins[i];
                            evitCache(cache, input.hash.toString('hex'), input.index);
                        }

                        txHash = convertEndianness(txHash);

                        for (var i = 0; i < tx.builtTx.outs.length; i++) {
                            var output = tx.builtTx.outs[i];
                            var address = null;
                            try {
                                address = bitcoin.Address.fromOutputScript(output.script).toString();
                            } catch (e) {
                                continue;
                            }

                            var addressCache = cache[address];
                            if (addressCache == null) {
                                addressCache = [];
                                cache[address] = addressCache;
                            }
                            cacheItem(addressCache, txHash, i, output.value + "", output.script.buffer.toString("hex"));
                        }
                    }
                    fulfill(res.data);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.prototype.stampDocument = function (hash, callback) {
    var suburl = "/stamps";
    var payload = JSON.stringify({
        hash: hash
    });
    var option = this.generatePostOption(suburl, payload);

    if (callback) {
        axios(option)
            .then(function (res) {
                callback(null, res.data.stampid)
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    fulfill(res.data.stampid);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.prototype.getStamp = function (stampId, callback) {
    var suburl = "/stamps/" + stampId;
    var option = this.generateOption(suburl);
    if (callback) {
        axios(option)
            .then(function (res) {
                callback(null, res.data);
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    fulfill(res.data);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.prototype.getContractStatus = function (contractID, callback) {
    var suburl = "/contracts/" + contractID + "/status";
    var option = this.generateOption(suburl);
    if (callback) {
        axios(this.generateOption(suburl))
            .then(function (res) {
                callback(null, res.data);
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    fulfill(res.data);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.prototype.queryContract = function (contractID, type, query, callback) {
    var suburl = "/contracts/" + contractID + "/query";
    var payload = "{\"query\":{\"type\":\"" + type + "\",\"body\":\"" + new Buffer(query).toString('base64') + "\"}}"
    var option = this.generatePostOption(suburl, payload);

    if (callback) {
        axios(option)
            .then(function (res) {
                callback(null, res.data)
            })
            .catch(function (res) {
                if (res instanceof Error) {
                    callback(res)
                } else {
                    callback(newCoinstackError(res.data, res.status));
                }
            });
    } else {
        return new Promise(function (fulfill, reject) {
            axios(option)
                .then(function (res) {
                    fulfill(res.data.stampid);
                })
                .catch(function (res) {
                    if (res instanceof Error) {
                        reject(res)
                    } else {
                        reject(newCoinstackError(res.data, res.status));
                    }
                });
        });
    }
}

Client.Math = BitcoinMath
Client.Util = util
Client.ECDSA = ECDSA
Client.ECKey = ECKey
Client.Transaction = Transaction


// transaction processing
Client.prototype.createTransactionBuilder = function () {
    return new TransactionBuilder(this);
}

Client.prototype.createLuaContractBuilder = function () {
    return new LuaContractBuilder(this);
}

module.exports = Client;