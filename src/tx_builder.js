var Client = require('./client.js')
var BitcoinMath = require('./math.js')
var Transaction = require('./tx.js')
var util = require('./util.js')
var _ = require('underscore');
var Promise = require('promise');

function TransactionBuilder(client) {
    this.client = client
    this.outputs = []
    this.wallets = []
    this.fee = BitcoinMath.toSatoshi("0.0001") // default fee
}

TransactionBuilder.prototype.setInput = function(fromAddress) {
    if (this.wallets.length > 0) {
        throw new Error("Multiple source support not implemented")
    }
    this.wallets.push({
        address: fromAddress
    })
}

TransactionBuilder.prototype.addOutput = function(toAddress, amount) {
    this.outputs.push({
        address: toAddress,
        amount: amount
    });
    return this
}

TransactionBuilder.prototype.setDataOutput = function(data) {
    var dataOutput = null
    if (data instanceof Buffer) {
        dataOutput = data
    } else if (typeof data === 'string' || data instanceof String) {
        // parse as hex
        dataOutput = new Buffer(data, "hex")
    }

    this.dataOutput = dataOutput

    return this
}

TransactionBuilder.prototype.setFee = function(amount) {
    this.fee = amount
    return this
}

function convertEndianness(hex) {
    hex = hex.replace(/^(.(..)*)$/, "0$1"); // add a leading zero if needed
    var res = hex.match(/../g); // split number in groups of two
    res.reverse(); // reverse the groups
    return res.join("");
}

TransactionBuilder.prototype.buildTransaction = function(callback) {
    if (this.wallets.length < 1) {
        throw new Error("Need at least one wallet to build transaction")
    }
    if (this.outputs.length < 1 && this.dataOutput == null) {
        throw new Error("Transaction output not specified")
    }

    var targetAmount = 0;
    _.each(this.outputs, function(output) {
        targetAmount += output.amount
    })
    targetAmount += this.fee
    var fee = this.fee;

    var bitcoin = util.bitcoin()
    var bitcoinTx = new bitcoin.TransactionBuilder();
    var fromAddress = this.wallets[0].address
    var outputs = this.outputs
    var dataOutput = this.dataOutput
    var client = this.client;

    if (callback) {
        client.getUnspentOutputs(fromAddress, function(err, uxtos) {
            if (err != null) {
                callback(err)
                return
            }

            // populate uxto
            var paymentTotal = 0;
            var outputCount = 0;

            var previousBalance = 0;
            _.each(uxtos, function(output) {
                if (paymentTotal < targetAmount) { // && (output.confirmations > 0 || output.change)) {
                    paymentTotal = paymentTotal + parseInt(output.value);
                    bitcoinTx.addInput(convertEndianness(output.transaction_hash), output.index);
                    outputCount = outputCount + 1;
                }

                previousBalance += parseInt(output.value);
            });

            if (paymentTotal < targetAmount) {
                callback(new Error("Not enough balance"), null);
                return
            }

            // add output
            _.each(outputs, function(output) {
                bitcoinTx.addOutput(output.address, output.amount);
            });

            if (null != dataOutput) {
                var dataScript = util.bitcoin().scripts.nullDataOutput(dataOutput)
                bitcoinTx.addOutput(dataScript, 0)
            }

            // add change
            if (paymentTotal - targetAmount > 0) {
                bitcoinTx.addOutput(fromAddress, paymentTotal - targetAmount);
            }

            // calculate resulting balance
            var resultingBalance = previousBalance;
            _.each(outputs, function(output) {
                if (fromAddress != output.address) {
                    resultingBalance -= output.amount;
                }
            });
            resultingBalance -= fee;

            callback(null, new Transaction(bitcoinTx), {
                previous: previousBalance,
                resulting: resultingBalance
            });
            return
        });
    } else {
        // return promise
        return new Promise(function(fulfill, reject) {
            client.getUnspentOutputs(fromAddress, function(err, uxtos) {
                if (err != null) {
                    reject(err)
                    return
                }

                // populate uxto
                var paymentTotal = 0;
                var outputCount = 0;
                var previousBalance = 0;
                _.each(uxtos, function(output) {
                    if (paymentTotal < targetAmount) { // && (output.confirmations > 0 || output.change)) {
                        paymentTotal = paymentTotal + parseInt(output.value);
                        bitcoinTx.addInput(convertEndianness(output.transaction_hash), output.index);
                        outputCount = outputCount + 1;
                    }

                    previousBalance += output.value;
                });

                if (paymentTotal < targetAmount) {
                    reject(new Error("Not enough balance"));
                    return
                }

                // add output
                _.each(outputs, function(output) {
                    bitcoinTx.addOutput(output.address, output.amount);
                });

                if (null != dataOutput) {
                    var dataScript = util.bitcoin().scripts.nullDataOutput(dataOutput)
                    bitcoinTx.addOutput(dataScript, 0)
                }

                // add change
                if (paymentTotal - targetAmount > 0) {
                    bitcoinTx.addOutput(fromAddress, paymentTotal - targetAmount);
                }

                // calculate resulting balance
                var resultingBalance = previousBalance;
                _.each(outputs, function(output) {
                    if (fromAddress != output.address) {
                        resultingBalance -= output.amount;
                    }
                });
                resultingBalance -= fee;
                fulfill({
                    tx: new Transaction(bitcoinTx),
                    balance: {
                        previous: previousBalance,
                        resulting: resultingBalance
                    }
                });
                return
            });
        });
    }
}

module.exports = TransactionBuilder;