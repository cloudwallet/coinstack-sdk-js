var bitcoin = require('bitcoinjs-lib')
var crypto = require('crypto')
var BigInteger = require('bigi')

	
var Util = {

}

// FIXME: workaround for setting custom bitcoinjs library from global (coinstack_user_set_bitcoin)
Util.bitcoin = function() {
    if (typeof coinstack_user_set_bitcoin === 'undefined') {
        return bitcoin
    } else {
        return coinstack_user_set_bitcoin
    }
}

Util.createKeyFromHexSeed = function (seed) {
    return Util.bitcoin().HDNode.fromSeedHex(seed, CoinStack.Util.bitcoin().networks.bitcoin).privKey.toWIF();
};

Util.convertEndianness = function(hex) {
    hex = hex.replace(/^(.(..)*)$/, "0$1"); // add a leading zero if needed
    var res = hex.match(/../g); // split number in groups of two
    res.reverse(); // reverse the groups
    return res.join("");
};

Util.hashSha256 = function(messageString) {
	var hash = Util.bitcoin().crypto.sha256(messageString).toString('hex')
	return hash
};

Util.sha256 = Util.bitcoin().crypto.sha256;
Util.ripemd160 = Util.bitcoin().crypto.ripemd160;
Util.hash160 = Util.bitcoin().crypto.hash160;
Util.hash256 = Util.bitcoin().crypto.hash256;

Util.toHex = function(buffer) {
    return buffer.toString('hex');
};

Util.convertToAddress = function(messageString) {
    var hash = Util.hash160(messageString);
    var mainnet = 0x00;
    return new (Util.bitcoin().Address)(hash, mainnet).toBase58Check();
};
Util.convertToPrivateKey = function(messageString) {
    var hash = Util.hash256(messageString);
    var d = BigInteger.fromBuffer(hash);
    var compressed = true;
    return new (Util.bitcoin().ECKey)(d, compressed).toWIF();
};

module.exports = Util;