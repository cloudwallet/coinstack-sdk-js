PACKAGE_VERSION=$(grep -m1 version package.json | awk -F: '{ print $2 }' | sed 's/[", ]//g') 
browserify ./index.js --standalone CoinStack | uglifyjs > ./dist/coinstack-$PACKAGE_VERSION.min.js
