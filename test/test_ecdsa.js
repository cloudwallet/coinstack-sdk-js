var assert = require('chai').assert
var assert = require('chai').assert
var expect = require('chai').expect
var Client = require('../src/client.js')
var bitcoin = require('bitcoinjs-lib')

suite('testing ECDSA', function() {
    var apiKey = "eb90dbf0-e98c-11e4-b571-0800200c9a66"
    var secretKey = "f8bd5b50-e98c-11e4-b571-0800200c9a66"
    var client = new Client(apiKey, secretKey, "mainnet.cloudwallet.io", "https")
    
    test('testing signToHash and verifySignature', function(done) {
        this.timeout(10000);
        var payload = "test is test"
        var signature = Client.ECDSA.sign("Kwg7NfVRrnrDUehdE9hn3qEZ51Tfk7rdr6rmyoHvjhRhoZE1KVkd", payload);
        var pubkey = Client.ECKey.derivePubKey("Kwg7NfVRrnrDUehdE9hn3qEZ51Tfk7rdr6rmyoHvjhRhoZE1KVkd")
        assert.isNotNull(signature);
        var res = Client.ECDSA.verifySignature(pubkey, payload, signature);
        assert.isTrue(res)
        done();
    });
    
    test('testing signToMessage and verifySignature', function(done) {
        this.timeout(10000);
        var message = "this is a test"
        var address = Client.ECKey.deriveAddress("Kwg7NfVRrnrDUehdE9hn3qEZ51Tfk7rdr6rmyoHvjhRhoZE1KVkd")
        var signature = Client.ECDSA.signMessage("Kwg7NfVRrnrDUehdE9hn3qEZ51Tfk7rdr6rmyoHvjhRhoZE1KVkd", message);
       // var signature = "H4uYYncnZ0wBSeZJ7BFsRVA76hwuOYAAJI5AfngS2Q+sjl7LDdLLCtwpVBCyq7J5+oS4jbvDP1o2fauqV163teQ="
        //var text = "Hello Bro"
        //var address = "14hV8B8vQRDz6WPWkSe2FMimyr7qYJQBZ5"
        console.log(signature);
        console.log(address);
        var res = Client.ECDSA.verifyMessageSignature(address, message, signature);
        assert.isTrue(res)
        done();
    });
});