var chai = require('chai'),
    chaiStats = require('chai-stats');
chai.use(chaiStats);
var assert = chai.assert
var expect = chai.expect
var BitcoinMath = require('../src/math.js')
var Client = require('../src/client.js')

var key = "L2FjRfrnd5F4fBk8FUGKeEr8ozvM5mnCpkyPCF6drUpZQNpbY6EH";
var address = "19FuCACUFvqore2DunFPqxW3p3bxyuXodr"

suite('testing coinstack transaction builder', function () {
    var client = new Client("", "", "testchain.blocko.io", "http")

    // test('testing creatingTransaction', function (done) {
    //     this.timeout(10000);

    //     var txBuilder = client.createTransactionBuilder();
    //     txBuilder.addOutput(address, Client.Math.toSatoshi("0.0001"))
    //     txBuilder.setInput(address);
    //     txBuilder.setFee(Client.Math.toSatoshi("0.0001"));
    //     txBuilder.buildTransaction(function (err, tx, balance) {
    //         try {
    //             assert.isNull(err)
    //             tx.sign(key)
    //             var hash = tx.getHash()
    //             assert.isNotNull(hash);
    //             // console.log(hash)
    //             var rawTx = tx.serialize()
    //             assert.isNotNull(rawTx);
    //             // console.log(rawTx);
    //             console.log(balance.previous);
    //             console.log(balance.resulting);
    //             assert.almostEqual(balance.previous - Client.Math.toSatoshi("0.0001"), balance.resulting);

    //             done();
    //         } catch (e) {
    //             done(e);
    //         }
    //         // send tx
    //         // client.sendTransaction(rawTx, function(err) {
    //         //     assert.isNull(err)
    //         //     done()
    //         // });
    //     })
    // });

    // test('testing creatingTransaction promise', function (done) {
    //     this.timeout(10000);

    //     var txBuilder = client.createTransactionBuilder();
    //     txBuilder.addOutput(address, Client.Math.toSatoshi("0.0001"))
    //     txBuilder.setInput(address);
    //     txBuilder.setFee(Client.Math.toSatoshi("0.0001"));

    //     var doneCounter = 0;
    //     txBuilder.buildTransaction().then(function (res) {
    //         try {
    //             res.tx.sign(key)
    //             var hash = res.tx.getHash()
    //             assert.isNotNull(hash);
    //             var rawTx = res.tx.serialize()
    //             assert.isNotNull(rawTx);
    //             assert.almostEqual(res.balance.previous - Client.Math.toSatoshi("0.0001"), res.balance.resulting);
    //             done();
    //         } catch (e) {
    //             done(e);
    //         }
    //     }).catch(function (err) {
    //         assert.isNull(err)
    //         done(err);
    //     })
    // });

    // test('testing creatingTransaction with not enough balance', function (done) {
    //     this.timeout(10000);

    //     var txBuilder = client.createTransactionBuilder();
    //     txBuilder.addOutput("1Gg95o3E89tmrLyUyZfq2xTLhetjNqy168", Client.Math.toSatoshi("0.0001"))
    //     txBuilder.addOutput("1Gg95o3E89tmrLyUyZfq2xTLhetjNqy168", Client.Math.toSatoshi("0.0001234"))
    //     txBuilder.addOutput("1Gg95o3E89tmrLyUyZfq2xTLhetjNqy168", Client.Math.toSatoshi("100.0001234"))
    //     txBuilder.setInput(address);
    //     txBuilder.buildTransaction(function (err, tx) {
    //         try {
    //             assert.isNotNull(err)
    //             done();
    //         } catch (e) {
    //             done(e);
    //         }
    //     })
    // });

    // test('testing creatingTransaction with not enough balance promise', function (done) {
    //     this.timeout(10000);

    //     var txBuilder = client.createTransactionBuilder();
    //     txBuilder.addOutput("1Gg95o3E89tmrLyUyZfq2xTLhetjNqy168", Client.Math.toSatoshi("0.0001"))
    //     txBuilder.addOutput("1Gg95o3E89tmrLyUyZfq2xTLhetjNqy168", Client.Math.toSatoshi("0.0001234"))
    //     txBuilder.addOutput("1Gg95o3E89tmrLyUyZfq2xTLhetjNqy168", Client.Math.toSatoshi("100.0001234"))
    //     txBuilder.setInput(address);
    //     txBuilder.buildTransaction().then(function (tx) {}).catch(function (e) {
    //         assert.isNotNull(e);
    //         done();
    //     });
    // });

    test('testing creatingTransaction with data', function (done) {
        this.timeout(10000);

        client = new Client("", "", "testchain.blocko.io", "http", {
            cacheUtxo: true
        })

        var txBuilder = client.createTransactionBuilder();
        txBuilder.setDataOutput(new Buffer("hello world"))
        txBuilder.setInput(address);
        txBuilder.buildTransaction(function (err, tx) {
            assert.isNull(err)
            tx.sign(key)
            var rawTx = tx.serialize()

            // send tx
            client.sendTransaction(rawTx, function (err) {
                console.log(err);
                assert.isNull(err)
                done()
            });
        })
    });

    test('testing creatingTransaction chaining and buffer', function (done) {
        this.timeout(10000);

        client = new Client("", "", "testchain.blocko.io", "http", {
            cacheUtxo: true
        })

        var txBuilder = client.createTransactionBuilder();
        txBuilder.addOutput(address, Client.Math.toSatoshi("0.0001"))
        txBuilder.setInput(address);
        txBuilder.setFee(Client.Math.toSatoshi("0.0001"));

        var doneCounter = 0;
        txBuilder.buildTransaction().then(function (res) {
            try {
                res.tx.sign(key)
                var hash = res.tx.getHash()
                assert.isNotNull(hash);
                var rawTx = res.tx.serialize()
                assert.isNotNull(rawTx);
                assert.almostEqual(res.balance.previous - Client.Math.toSatoshi("0.0001"), res.balance.resulting);


                client.sendTransaction(rawTx, function (err) {
                    assert.isNull(err)
                    txBuilder = client.createTransactionBuilder();
                    txBuilder.addOutput(address, Client.Math.toSatoshi("0.0001"))
                    txBuilder.setInput(address);
                    txBuilder.setFee(Client.Math.toSatoshi("0.0001"));

                    txBuilder.buildTransaction().then(function (res) {
                        res.tx.sign(key)
                        var rawTx2 = res.tx.serialize()

                        client.sendTransaction(rawTx2, function (err) {
                            txBuilder = client.createTransactionBuilder();
                            txBuilder.addOutput(address, Client.Math.toSatoshi("0.0001"))
                            txBuilder.setInput(address);
                            txBuilder.setFee(Client.Math.toSatoshi("0.0001"));

                            txBuilder.buildTransaction().then(function (res) {
                                res.tx.sign(key)
                                var rawTx3 = res.tx.serialize()

                                client.sendTransaction(rawTx3, function (err) {
                                    console.log(err);
                                    done();
                                });
                            }).catch(function (err) {
                                assert.isNull(err);
                                done(err);
                            });
                        });

                    }).catch(function (err) {
                        assert.isNull(err);
                        done(err);
                    });
                });
            } catch (e) {
                done(e);
            }
        }).catch(function (err) {
            assert.isNull(err)
            done(err);
        })
    });



    // test('testing creatingTransaction with data', function(done) {
    //     this.timeout(10000);
    //     var txBuilder = client.createTransactionBuilder();

    //     expect(function() {
    //         txBuilder.setDataOutput(new Buffer("hello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21dhello world sdasdd21d"))
    //     }).to.throw(Error)

    //     done(); // exception is expected

    // });
});