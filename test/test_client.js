var assert = require('chai').assert
var assert = require('chai').assert
var expect = require('chai').expect
var Client = require('../src/client.js')
var bitcoin = require('bitcoinjs-lib')

suite('testing coinstack client', function() {
    var apiKey = "eb90dbf0-e98c-11e4-b571-0800200c9a66"
    var secretKey = "f8bd5b50-e98c-11e4-b571-0800200c9a66"
    var client = new Client(apiKey, secretKey, "mainnet.cloudwallet.io", "https")
    test('testing getBlockchain Status', function(done) {
        this.timeout(10000);

        client.getBlockchainStatus(function(err, status) {
            try {
                assert.isNull(err)
                assert.isNotNull(status)
                assert.property(status, "best_block_hash")
                assert.property(status, "best_height")
                done()
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing getBlockchain Status Promise', function(done) {
        this.timeout(10000);
        client.getBlockchainStatus().then(function(status) {
            assert.isNotNull(status)
            assert.property(status, "best_block_hash")
            assert.property(status, "best_height")
            done();
        }).catch(function(err) {
            assert.iNull(err)
            done();
        });
    });

    test('testing getTransaction', function(done) {
        this.timeout(10000);

        client.getTransaction("001f5eba608a84ba97dd7ac1b21b822b74c91ffbd75c42c7b88abe178f632b31", function(err, tx) {
            try {
                assert.isNull(err)
                assert.isNotNull(tx)
                assert.property(tx, "transaction_hash")
                assert.property(tx, "block_hash")
                assert.property(tx, "coinbase")
                assert.property(tx, "inputs")
                assert.property(tx, "outputs")
                assert.property(tx, "time")
                assert.property(tx, "addresses")
                done();
            } catch (err) {
                assert.isNull(err)
                done(e);
            }
        });
    });

    test('testing getTransaction Promise', function(done) {
        this.timeout(10000);

        client.getTransaction("001f5eba608a84ba97dd7ac1b21b822b74c91ffbd75c42c7b88abe178f632b31").then(function(tx) {
            assert.isNotNull(tx)
            assert.property(tx, "transaction_hash")
            assert.property(tx, "block_hash")
            assert.property(tx, "coinbase")
            assert.property(tx, "inputs")
            assert.property(tx, "outputs")
            assert.property(tx, "time")
            assert.property(tx, "addresses")
            done();
        }).catch(function(err) {
            assert.isNull(err)
            done(e);
        });
    });

    test('testing getTransaction wth error Promise', function(done) {
        this.timeout(10000);

        client.getTransaction("WRONGTX").then(function(tx) {
            assert.isNull(tx)
            done();
        }).catch(function(err) {
            assert.isNotNull(err)
            assert.property(err, "errorType")
            assert.property(err, "errorCode")
            assert.property(err, "message")
            assert.isFalse(err.retry)
            done();
        });
    });

    test('testing getTransaction with OP_RETURN', function(done) {
        this.timeout(10000);

        client.getTransaction("d935fbeb8d492b890a5081860d415b65d40496b9ecd23284641c3b5d936cebd9", function(err, tx) {
            try {
                assert.isNull(err)
                assert.isNotNull(tx)
                assert.property(tx, "transaction_hash")
                assert.property(tx, "block_hash")
                assert.property(tx, "coinbase")
                assert.property(tx, "inputs")
                assert.property(tx, "outputs")
                assert.property(tx, "time")
                assert.property(tx, "addresses")
                assert.property(tx.outputs[1], "data")
                done()
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing getBlock', function(done) {
        this.timeout(10000);

        client.getBlock("000000000000000006ae5d6d96b99181b52ec2b829c9994ab7fd4ab9a6389212", function(err, block) {
            try {
                assert.isNull(err)
                assert.isNotNull(block)
                assert.property(block, "block_hash")
                assert.property(block, "confirmation_time")
                assert.property(block, "parent")
                assert.property(block, "children")
                assert.property(block, "transaction_list")
                done()
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing getBlock Promise', function(done) {
        this.timeout(10000);

        client.getBlock("000000000000000006ae5d6d96b99181b52ec2b829c9994ab7fd4ab9a6389212").then(function(block) {
            assert.isNotNull(block)
            assert.property(block, "block_hash")
            assert.property(block, "confirmation_time")
            assert.property(block, "parent")
            assert.property(block, "children")
            assert.property(block, "transaction_list")
            done();
        }).catch(function(e) {
            done(e);
        });;
    });

    test('testing getBalance', function(done) {
        this.timeout(10000);

        client.getBalance("17tz7JRkxkzhkCnqtnyYXu8sk4E8QwZUqh", function(err, balance) {
            try {
                assert.isNull(err)
                assert.isNotNull(balance)
                assert.isNumber(balance)
                done()
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing getBalance Promise', function(done) {
        this.timeout(10000);

        client.getBalance("17tz7JRkxkzhkCnqtnyYXu8sk4E8QwZUqh").then(function(balance) {
            assert.isNotNull(balance)
            assert.isNumber(balance)
            done()
        }).catch(function(e) {
            done(e);
        });
    });

    test('testing getTransactions', function(done) {
        this.timeout(10000);

        client.getTransactions("17tz7JRkxkzhkCnqtnyYXu8sk4E8QwZUqh", function(err, transactions) {
            try {
                assert.isNull(err)
                assert.isNotNull(transactions)
                assert.isArray(transactions)
                done()
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing getTransactions Promises', function(done) {
        this.timeout(10000);

        client.getTransactions("17tz7JRkxkzhkCnqtnyYXu8sk4E8QwZUqh").then(function(transactions) {
            assert.isNotNull(transactions)
            assert.isArray(transactions)
            done()
        }).catch(function(e) {
            done(e);
        });
    });

    test('testing getUxto', function(done) {
        this.timeout(10000);

        client.getUnspentOutputs("17tz7JRkxkzhkCnqtnyYXu8sk4E8QwZUqh", function(err, uxtos) {
            try {
                assert.isNull(err)
                assert.isNotNull(uxtos)
                assert.isArray(uxtos)
                assert.isTrue(uxtos.length > 0)
                assert.property(uxtos[0], "transaction_hash")
                assert.property(uxtos[0], "index")
                assert.property(uxtos[0], "value")
                assert.property(uxtos[0], "script")
                assert.property(uxtos[0], "confirmations")
                done()
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing getUxto Promise', function(done) {
        this.timeout(10000);

        client.getUnspentOutputs("17tz7JRkxkzhkCnqtnyYXu8sk4E8QwZUqh").then(function(uxtos) {
            assert.isNotNull(uxtos)
            assert.isArray(uxtos)
            assert.isTrue(uxtos.length > 0)
            assert.property(uxtos[0], "transaction_hash")
            assert.property(uxtos[0], "index")
            assert.property(uxtos[0], "value")
            assert.property(uxtos[0], "script")
            assert.property(uxtos[0], "confirmations")
            done()
        }).catch(function(e) {
            done(e);
        });
    });

    test('testing sendTx', function(done) {
        this.timeout(10000);
        client.sendTransaction("010000000124398225cf3d515a7ef7e816c37cbfd1cae9e01b401b90192c4dd479d23e7eab000000006b483045022100a8b331d506e265e79feb535a51dd5fbcd2724f0f6a1482cbea38d772ceae4e8c02206d7ee4bf2af3f8289310a09cc9760df6ae4768e24238cc13aee1739b837900ea012102ce3b0c53a06262e2a64e0639f2901447c2288ab437b5317fe05848e92a2ba25fffffffff0216120100000000001976a91415aad25727498a360e92eeb96db26f55fb38edcb88ac10270000000000001976a914abf0db3809c8ae1697f067a5c92171fd6ca3aaa988ac00000000", function(err) {
            try {
                assert.isNotNull(err)
                done()
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing stamping', function(done) {
        this.timeout(10000);
        client.stampDocument("b989b847b4189ab3805f7f600b9c1ccc2cec1e7302d86f93c75733fa1fb4ea18", function(err, stampid) {
            try {
                assert.isNull(err);
                assert.isNotNull(stampid);
                done()
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing stamping promise', function(done) {
        this.timeout(10000);
        client.stampDocument("b989b847b4189ab3805f7f600b9c1ccc2cec1e7302d86f93c75733fa1fb4ea18")
            .then(function(stampid) {
                assert.isNotNull(stampid);
                done();
            }).catch(function(e) {
                done(e);
            });
    });

    test('testing getStamp', function(done) {
        this.timeout(10000);
        client.getStamp("f0276c7a47b83e9af575047dc4674237772e4d1ea2c7db9aeb1e3e2f049b28b8-0", function(err, stamp) {
            try {
                assert.isNull(err);
                assert.isNotNull(stamp);
                done()
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing getStamp promise', function(done) {
        this.timeout(10000);
        client.getStamp("f0276c7a47b83e9af575047dc4674237772e4d1ea2c7db9aeb1e3e2f049b28b8-0")
            .then(function(stamp) {
                assert.isNotNull(stamp);
                done();
            }).catch(function(e) {
                done(e);
            });
    });

    test('testing getStamp promise with error', function(done) {
        this.timeout(10000);
        client.getStamp("f0276c7a47b83e9af575047dc4674237772e4d1ea2c7db9aeb1e3e2f049b28b8-")
            .then(function(stamp) {
                assert.isNull(stamp);
            }).catch(function(err) {
                assert.isNotNull(err)
                assert.property(err, "errorType")
                assert.property(err, "errorCode")
                assert.property(err, "message")
                assert.isFalse(err.retry)
                done();
            });
    });


    test('testing sendTx promise', function(done) {
        this.timeout(10000);
        client.sendTransaction("010000000124398225cf3d515a7ef7e816c37cbfd1cae9e01b401b90192c4dd479d23e7eab000000006b483045022100a8b331d506e265e79feb535a51dd5fbcd2724f0f6a1482cbea38d772ceae4e8c02206d7ee4bf2af3f8289310a09cc9760df6ae4768e24238cc13aee1739b837900ea012102ce3b0c53a06262e2a64e0639f2901447c2288ab437b5317fe05848e92a2ba25fffffffff0216120100000000001976a91415aad25727498a360e92eeb96db26f55fb38edcb88ac10270000000000001976a914abf0db3809c8ae1697f067a5c92171fd6ca3aaa988ac00000000")
            .then(function(err) {
                done()
            }).catch(function(e) {
                assert.isNotNull(e);
                done();
            });
    });


});