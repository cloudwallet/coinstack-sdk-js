var assert = require('chai').assert
var assert = require('chai').assert
var expect = require('chai').expect
var Client = require('../src/client.js')
var bitcoin = require('bitcoinjs-lib')

suite('testing multisig tx', function() {
    var apiKey = "eb90dbf0-e98c-11e4-b571-0800200c9a66"
    var secretKey = "f8bd5b50-e98c-11e4-b571-0800200c9a66"
    var client = new Client(apiKey, secretKey, "mainnet.cloudwallet.io", "https")

    test('testing create a 2-of-3 multisig P2SH address', function(done) {
        // FIXME: expose this functionality from SDK
        this.timeout(10000);
        var pubKeys = [
            '04cbc882d221f567005ea61aa45d5414f25371472f6ab5973e13a39a9edc26359b6980aa4f6f34cea62e82bbe13adc7fde9fc26bba2be2e7c5f8011a68bea39bae',
            '0494bae4aa9a4c2ca6103899098ca0867f62ca24af02fee2d6473a698d92fbc8c449aa2e236c0684ebb9e0fbb23d847d4624fd8ca4a1fdc940df432c6e312e18e8',
            '0468806910b7a3589f40c09092d3a45c64f1ef950e23d4b5aa92ad4c3de7804ed95f0f50aca9ae928fb6e00223fad667693bf3e2b716dd6c9d474ad79f5b7a107e'
        ].map(function(hex) {
            return new Buffer(hex, 'hex')
        })
        pubKeys[0] = bitcoin.ECPubKey.fromBuffer(pubKeys[0])
        pubKeys[1] = bitcoin.ECPubKey.fromBuffer(pubKeys[1])
        pubKeys[2] = bitcoin.ECPubKey.fromBuffer(pubKeys[2])

        var redeemScript = bitcoin.scripts.multisigOutput(2, pubKeys) // 2 of 3
        var scriptPubKey = bitcoin.scripts.scriptHashOutput(redeemScript.getHash())
        var address = bitcoin.Address.fromOutputScript(scriptPubKey).toString()

        assert.isNotNull(address)
        done()
    });

    test('testing creatingMultiSigTransaction', function(done) {
        this.timeout(10000);

        var txBuilder = client.createTransactionBuilder();
        txBuilder.addOutput("1F444Loh6KzUQ8u8mAsz5upBtQ356vN95s", Client.Math.toSatoshi("0.0001"))
        txBuilder.setInput("357UeWvhR2xK9hUEdVnrxf1Kkbf6B1wLGT");
        txBuilder.setFee(Client.Math.toSatoshi("0.0001"));
        var redeemScript = "52410468806910b7a3589f40c09092d3a45c64f1ef950e23d4b5aa92ad4c3de7804ed95f0f50aca9ae928fb6e00223fad667693bf3e2b716dd6c9d474ad79f5b7a107e410494bae4aa9a4c2ca6103899098ca0867f62ca24af02fee2d6473a698d92fbc8c449aa2e236c0684ebb9e0fbb23d847d4624fd8ca4a1fdc940df432c6e312e18e84104cbc882d221f567005ea61aa45d5414f25371472f6ab5973e13a39a9edc26359b6980aa4f6f34cea62e82bbe13adc7fde9fc26bba2be2e7c5f8011a68bea39bae53ae";
        txBuilder.buildTransaction(function(err, tx) {
            try {
                assert.isNull(err)
                var privateKeys = ["5Jh46BhaJJeiW8C9tTz6ockGEgYnLYfrJmGnwYdcDpBbAvWvCbv",
                    //"5JSad8KB82c3XW69e1hz8g1YFFts4GTdjHuWHkh4d4A8MZWw12N",
                    "5JSad8KB82c3XW69e1hz8g1YFFts4GTdjHuWHkh4d4A8MZWw12N"
                ]
                tx.signMultisig(privateKeys, redeemScript)
                var hash = tx.getHash()
                assert.isNotNull(hash)
                    // console.log(hash)
                var rawTx = tx.serialize()
                    //console.log(rawTx);
                    // to-do
                assert.isNotNull(rawTx);
                done();
            } catch (e) {
                done(e);
            }
            // send tx
            // client.sendTransaction(rawTx, function(err) {
            //     assert.isNull(err)
            //     done()
            // });
        })
    });

    test('testing creatingMultiSigTransactionPartialSign', function(done) {
        this.timeout(10000);

        var txBuilder = client.createTransactionBuilder();
        txBuilder.addOutput("1F444Loh6KzUQ8u8mAsz5upBtQ356vN95s", Client.Math.toSatoshi("0.0001"))
        txBuilder.setInput("3EevY8SMDujvoqNJWV1XmshMD8UxXhJz1c");
        txBuilder.setFee(Client.Math.toSatoshi("0.0001"));
        var redeemScript = "524104162a5b6239e12d3d52f2c880555934525dbb014dae7165380f77dcbf58b121b8033f59a1f7a4dcea589fc4405ac756542dfa393d53f7a559038f59b8d1084de541046a8fca1041f6ecf55aaa4e431b6c4ee72b51492330e777f2967697eb633e277eabf5d6e2ab3132b218a2d03b013ac90a80a4a2b5a27d1fa2a78cccad64d43b6f4104e850211b270fe7c97335411fcb774f6c7af0a8dd2e3360ba577e0c2979c51a375f5c256e2c8701d1b9777c15b7fc8b42af435977fe338e4a4e19683c884ad0fd53ae";
        txBuilder.buildTransaction(function(err, tx) {
            try {
                assert.isNull(err)
                var privateKeys = ["5JiqywVBWDphZbR2UWnUtj3yTX52LmGhnBy8gGED7GDdxzPuRaZ"
                    //"5J4ZadEdMs3zaqTutP1eQoKnCGKSYrUgkPwBZrk3hNmFiz7B6Ke",
                    //"5JKhaPecauUSKKZTJ2R8zhNZqFxLSDu3Q5dPU3ijSqkf2WGVekn"
                ]
                tx.signIncompleteMultisig(privateKeys, redeemScript)
                var hash = tx.getHash()
                assert.isNotNull(hash)
                    // console.log(hash)
                var rawTx = tx.serialize()
                    //console.log("1 : " + rawTx);
                    ///////////////////////////////////// add another sign //////////////////
                    //tx2.fromHex("010000000207aa72987b86d4d2f290bd8c211da5c22241c9b6a255ffef5fe9ed95665b4ab101000000fd150100483045022100e99bc1b0c40b549cb1985d0a8b294cb5502b2e2bea01459ef364b5e5a93709fe02207d0300005fb6e3294581e35b95b1c1579cbd2b83efb2aa9a8906eeac49e44966014cc9524104162a5b6239e12d3d52f2c880555934525dbb014dae7165380f77dcbf58b121b8033f59a1f7a4dcea589fc4405ac756542dfa393d53f7a559038f59b8d1084de541046a8fca1041f6ecf55aaa4e431b6c4ee72b51492330e777f2967697eb633e277eabf5d6e2ab3132b218a2d03b013ac90a80a4a2b5a27d1fa2a78cccad64d43b6f4104e850211b270fe7c97335411fcb774f6c7af0a8dd2e3360ba577e0c2979c51a375f5c256e2c8701d1b9777c15b7fc8b42af435977fe338e4a4e19683c884ad0fd53aeffffffff31a80c997793b1202f56a932384effff128fd370d58fbf725b4124b93621e65600000000fd150100483045022100c7cb3346e8c2e166a5b46c208e08864b5f5e3ef6b2fd19839eed0c805fbdd5d70220551353b4f141b252e5e22b8a95dfbc66e88801e0797043fb2397fce514f5f8a0014cc9524104162a5b6239e12d3d52f2c880555934525dbb014dae7165380f77dcbf58b121b8033f59a1f7a4dcea589fc4405ac756542dfa393d53f7a559038f59b8d1084de541046a8fca1041f6ecf55aaa4e431b6c4ee72b51492330e777f2967697eb633e277eabf5d6e2ab3132b218a2d03b013ac90a80a4a2b5a27d1fa2a78cccad64d43b6f4104e850211b270fe7c97335411fcb774f6c7af0a8dd2e3360ba577e0c2979c51a375f5c256e2c8701d1b9777c15b7fc8b42af435977fe338e4a4e19683c884ad0fd53aeffffffff01204e0000000000001976a9149a258fac5c9f2b79de327e7622b0c1e5783508cb88ac00000000");
                var tx2 = Client.Transaction.fromHex(rawTx);
                var privateKeys = [ //"5JiqywVBWDphZbR2UWnUtj3yTX52LmGhnBy8gGED7GDdxzPuRaZ",
                    //"5J4ZadEdMs3zaqTutP1eQoKnCGKSYrUgkPwBZrk3hNmFiz7B6Ke",
                    "5J4ZadEdMs3zaqTutP1eQoKnCGKSYrUgkPwBZrk3hNmFiz7B6Ke"
                ]
                tx2.signMultisig(privateKeys, redeemScript)
                hash = tx2.getHash()
                rawTx = tx2.serialize()
                    //console.log("2 : " + rawTx);

                assert.isNotNull(rawTx);
                done();
            } catch (e) {
                done(e);
            }
            // send tx
            // client.sendTransaction(rawTx, function(err) {
            //     assert.isNull(err)
            //     done()
            // });
        })
    });
});