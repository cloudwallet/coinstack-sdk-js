var chai = require('chai'),
    chaiStats = require('chai-stats');
chai.use(chaiStats);
var assert = chai.assert
var expect = chai.expect
var BitcoinMath = require('../src/math.js')
var Client = require('../src/client.js')

suite('testing coinstack lua contract builder', function () {
    var apiKey = "eb90dbf0-e98c-11e4-b571-0800200c9a66"
    var secretKey = "f8bd5b50-e98c-11e4-b571-0800200c9a66"
    var client = new Client(apiKey, secretKey, "127.0.0.1:3000", "http")

    test('testing creating contract', function (done) {
        this.timeout(10000);

        var builder = client.createLuaContractBuilder();
        builder = builder.setInput("1NdgqKo6MgyspmwnXmwNTFLCqmLdpoFowB").setContractID("1NdgqKo6MgyspmwnXmwNTFLCqmLdpoFowB");
        builder.setDefinition(`
    local system = require("system");
    function foo(msg)
        system.print(msg .. " from lua contract");
        return msg .. " from lua contract"
    end
            `);
        console.log(builder.getHash())
        builder.buildTransaction(function (err, tx) {
            try {
                assert.isNull(err);
                tx.sign("L1w5UHhbPfbUumGomryB88fA3ENZ5cNWuJmDxMNn9YSp7A4nJgJ8");
                var rawTx = tx.serialize();
                console.log(rawTx);
                assert.isNotNull(rawTx);
                client.sendTransaction(rawTx, function (err) {
                    assert.isNull(err)
                    done()
                });
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing invoking contract', function (done) {
        this.timeout(10000);

        var builder = client.createLuaContractBuilder();
        builder = builder.setInput("1NdgqKo6MgyspmwnXmwNTFLCqmLdpoFowB").setContractID("1NdgqKo6MgyspmwnXmwNTFLCqmLdpoFowB");
        builder.setExecution(`call("foo", "hello world")`);
        console.log(builder.getHash())
        builder.buildTransaction(function (err, tx) {
            try {
                assert.isNull(err);
                tx.sign("L1w5UHhbPfbUumGomryB88fA3ENZ5cNWuJmDxMNn9YSp7A4nJgJ8");
                var rawTx = tx.serialize();
                console.log(rawTx);
                assert.isNotNull(rawTx);
                client.sendTransaction(rawTx, function (err) {
                    assert.isNull(err)
                    done()
                });
            } catch (e) {
                done(e);
            }
        });
    });

    test('testing querying contract', function (done) {
        this.timeout(10000);
        client.queryContract("1NdgqKo6MgyspmwnXmwNTFLCqmLdpoFowB", "LSC", "return {1,2,3}", function (err, res) {
            console.log(err, res);
            done();
        });
    });

    test('testing querying contract status', function (done) {
        this.timeout(10000);
        client.getContractStatus("1NdgqKo6MgyspmwnXmwNTFLCqmLdpoFowB", function (err, res) {
            console.log(err, res);
            done();
        });
    });

});