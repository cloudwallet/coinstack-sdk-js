var assert = require('chai').assert
var expect = require('chai').expect
var Util = require('../src/util.js');
var ECKey = require('../src/eckey.js');

suite('testing Util', function() {
	var unit_digest = function(title, digest, source, expected) {
		test(title, function(done) {
			var result = Util.toHex(digest(source));
			console.log(title, result);
			expect(result).equals(expected);
			done();
		});
	};
	var unit_convert = function(title, convert, source, expected) {
		test(title, function(done) {
			var result = convert(source);
			console.log(title, result);
			expect(result).equals(expected);
			done();
		});
	};
	var convertToPrivateKeyAddress = function(source) {
		var privateKeyWIF = Util.convertToPrivateKey(source);
		var address = ECKey.deriveAddress(privateKeyWIF);
		return address;
	};


	var EMPTY_STRING = "";
	var SAMPLE_INPUT = "17zaqbwaDb9UVajcSjNxb2nCzjksortjqq";
	
	unit_digest("sha256", Util.sha256, EMPTY_STRING, "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
	unit_digest("ripemd160", Util.ripemd160, EMPTY_STRING, "9c1185a5c5e9fc54612808977ee8f548b2258d31");
	unit_digest("hash160", Util.hash160, EMPTY_STRING, "b472a266d0bd89c13706a4132ccfb16f7c3b9fcb");
	unit_digest("hash256", Util.hash256, EMPTY_STRING, "5df6e0e2761359d30a8275058e299fcc0381534545f55cf43e41983f5d4c9456");
	
	unit_digest("sha256", Util.sha256, SAMPLE_INPUT, "61c4fb3ab008f67c020dbf640fc1c89e52fda76dfcf2d62b66ab42c392687eab");
	unit_digest("ripemd160", Util.ripemd160, SAMPLE_INPUT, "246d2fe2fa62af0a7ae0e30d6c9f6c48330f0fd8");
	unit_digest("hash160", Util.hash160, SAMPLE_INPUT, "db02744856baf1ee91aa38feb12a840244387e82");
	unit_digest("hash256", Util.hash256, SAMPLE_INPUT, "5dcf67f9f5b73219b16b8ec97ddf86d3ccb014d0bb9875b4a1a08ae0638b3429");


	unit_convert("convertToAddress", Util.convertToAddress, EMPTY_STRING, "1HT7xU2Ngenf7D4yocz2SAcnNLW7rK8d4E");
	unit_convert("convertToPrivateKey", convertToPrivateKeyAddress, EMPTY_STRING, "1LCGKp8zkmU3jBSdsRNfLzqsJH3qSzSxyk");
	
	unit_convert("convertToAddress", Util.convertToAddress, SAMPLE_INPUT, "1Ly1vXxG93BVWmSzSnqTSUEkvcAVdoUmkJ");
	unit_convert("convertToPrivateKey", convertToPrivateKeyAddress, SAMPLE_INPUT, "1Fo94iXHbV8XcxivmuYqWu9G2CmEbKnZZx");
});